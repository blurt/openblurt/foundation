import React, { Fragment } from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';

// Custom Components
import Navbar from './components/layout/Navbar';
import Landing from './components/layout/Landing';
import Foundation from './components/pages/Foundation';
import About from './components/pages/About';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

const App = () => {
  return (
    <Router>
      <Fragment>
        <Navbar />
        <Landing />
        <Switch>
          <Route exact path='/foundation' component={Foundation} />
          <Route exact path='/about' component={About} />
        </Switch>
      </Fragment>
    </Router>
  );
}

export default App;
