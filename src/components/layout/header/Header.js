import React, { useEffect, Fragment } from 'react';
import Logo from '../../images/logo.png';

import './Header.css';

const Header = () => {
  useEffect(() => {
    window.scrollTo(0, 0);

    // List of sentences
    let _CONTENT = ['TOKENIZED', 'DECENTRALIZED', 'IMMUTABLE'];

    // Current sentence being processed
    let _PART = 0;

    // Character number of the current sentence being processed
    let _PART_INDEX = 0;

    // Holds the handle returned from setInterval
    let _INTERVAL_VAL;

    // Element that holds the text
    let _ELEMENT = document.querySelector('#text');

    // Implements typing effect
    function Type() {
      let text = _CONTENT[_PART].substring(0, _PART_INDEX + 1);
      _ELEMENT.innerHTML = text;
      _PART_INDEX++;

      // If full sentence has been displayed then start to delete the sentence after some time
      if (text === _CONTENT[_PART]) {
        clearInterval(_INTERVAL_VAL);
        setTimeout(function () {
          _INTERVAL_VAL = setInterval(Delete, 50);
        }, 1000);
      }
    }

    // Implements deleting effect
    function Delete() {
      let text = _CONTENT[_PART].substring(0, _PART_INDEX - 1);
      _ELEMENT.innerHTML = text;
      _PART_INDEX--;

      // If sentence has been deleted then start to display the next sentence
      if (text === '') {
        clearInterval(_INTERVAL_VAL);

        // If last sentence then display the first one, else move to the next
        if (_PART === _CONTENT.length - 1) _PART = 0;
        else _PART++;
        _PART_INDEX = 0;

        // Start to display the next sentence after some time
        setTimeout(function () {
          _INTERVAL_VAL = setInterval(Type, 100);
        }, 200);
      }
    }

    // Start the typing effect on load
    _INTERVAL_VAL = setInterval(Type, 100);
  });
  return (
    <Fragment>
      <div className='jumbo mb-0'>
        <div className='Header'>
          <div className='Header-Overlay'>
            <div className='content'>
              <div className='header-image'>
                <img src={Logo} alt='logo' className='logo' />
              </div>
              <h1 className="m-2">
                <span
                  className='typewriter'
                  style={{ color: '#ED2825' }}
                  id='text'
                />{' '}
                Blockchain
              </h1>
              <div className='btn-wrapper'>
                <a href='#!' className='btn btn-lg get-started-btn'>
                  Get Started
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 1440 320'>
        <path
          fill='#F3F4F5'
          fill-opacity='1'
          d='M0,160L48,176C96,192,192,224,288,234.7C384,245,480,235,576,208C672,181,768,139,864,106.7C960,75,1056,53,1152,64C1248,75,1344,117,1392,138.7L1440,160L1440,0L1392,0C1344,0,1248,0,1152,0C1056,0,960,0,864,0C768,0,672,0,576,0C480,0,384,0,288,0C192,0,96,0,48,0L0,0Z'
        ></path>
      </svg>
    </Fragment>
  );
};

export default Header;
