import React, { Fragment } from 'react';
import Header from './header/Header';
import SectionImageOne from '../images/section-image-one.png';
import SectionImageTwo from '../images/section-image-two.png';

const Landing = () => {
  return (
    <Fragment>
      <Header />
      <section>
        <div className='container'>
          <div className='row'>
            <div className='col-md-12'>
              <div
                className='section-header'
                style={{ paddingTop: '0px', paddingBottom: '90px' }}
              >
                <h1 className='text-center'>What is Blurt?</h1>
                <p>
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugit
                  incidunt asperiores repellat iusto, ipsum, debitis nihil quis
                  quisquam repellendus non similique neque ipsa iure cupiditate
                  dicta nulla sed cumque minus.
                </p>
              </div>
            </div>
          </div>
          <div className='row'>
            <div className='col-md-6'>
              <div className='section-image-wrapper'>
                <img
                  className='section-image'
                  src={SectionImageOne}
                  alt='choose'
                />
              </div>
            </div>
            <div className='col-md-6'>
              <div class='choose-us-title'>
                <h2> Why Choose Blurt?</h2>
                <p>
                  Cum sociis natoque penatibus et magnis dis parturient montes,
                  nascetur ridiculus mus. Quisque lacus dui, interdum sit amet
                  varius.
                </p>
              </div>

              <div className='choose-us-des'>
                <div
                  className='choose-us-item wow fadeInUp'
                  data-wow-duration='.5s'
                  data-wow-delay='1s'
                >
                  <div>
                    <i className='fa fa-bolt'></i>
                  </div>

                  <h3>Efficient</h3>
                  <p>
                    Lipsum Cum sociis natoque penatibus et magnis dis parturient
                    montes, nascetur ridiculus mus. Quisque lacus dui, interdum
                    sit amet varius a, cursus sit amet sapien. Donec eu placerat
                    nisi.
                  </p>
                </div>

                <div
                  className='choose-us-item wow fadeInUp'
                  data-wow-duration='.5s'
                  data-wow-delay='1s'
                >
                  <div>
                    <i className='fa fa-bullhorn'></i>
                  </div>

                  <h3>Scalable</h3>
                  <p>
                    Diso Lipsum Cum sociis natoque penatibus et magnis dis
                    parturient montes, nascetur ridiculus mus. Quisque lacus
                    dui, interdum sit amet varius a, cursus sit amet sapien.
                    Donec eu placerat nisi.
                  </p>
                </div>

                <div
                  className='choose-us-item wow fadeInUp'
                  data-wow-duration='.5s'
                  data-wow-delay='1s'
                >
                  <div>
                    <i className='fa fa-flag'></i>
                  </div>

                  <h3>No Downvotes</h3>
                  <p>
                    Sed ut perspiciatis Cupidatat non proident, sunt in culpa
                    qui officia deserunt mollit anim id est laborum.unde omnis
                    iste natus error sit.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className='features' id='FEATURES'>
        <div className='container'>
          <div className='row'>
            <div className='col-md-12'>
              <div className='section-header wow fadeIn' data-wow-duration='1s'>
                <h1> Features </h1>

                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                  Ut enim ad minim veniam, quis nostrud exercitation ullamco
                  laboris nisi ut aliquip.
                </p>
              </div>
            </div>
          </div>
        </div>

        <div className='features-inner-wrapper'>
          <div className='container'>
            <div className='row'>
              <div className='col-md-4 wow fadeInLeft' data-wow-duration='1s'>
                <div className='left-feature-item'>
                  <div className='left-feature-item-icon'>
                    <i className='fa fa-desktop'></i>
                  </div>

                  <h3> Feature One </h3>
                  <p>
                    Lorem ipsum dolor, consectetur sed do adipisicing elit, sed
                    do eiusmod tempor incididunt
                  </p>
                </div>

                <div className='left-feature-item'>
                  <div className='left-feature-item-icon'>
                    <i className='fa fa-mobile'></i>
                  </div>

                  <h3> Feature Two </h3>
                  <p>
                    Lorem ipsum dolor, consectetur sed do adipisicing elit, sed
                    do eiusmod tempor incididunt
                  </p>
                </div>

                <div className='left-feature-item'>
                  <div className='left-feature-item-icon'>
                    <i className='fa fa-eye'></i>
                  </div>

                  <h3> Feature Three</h3>
                  <p>
                    Lorem ipsum dolor, consectetur sed do adipisicing elit, sed
                    do eiusmod tempor incididunt
                  </p>
                </div>
              </div>

              <div className='col-md-4'>
                <div className='feature-iphone'>
                  <img
                  width='150'
                    className='wow bounceIn img-responsive'
                    data-wow-duration='1s'
                    src={SectionImageTwo}
                    alt='feature'
                  />
                </div>
              </div>

              <div className='col-md-4 wow fadeInRight' data-wow-duration='1s'>
                <div className='right-feature-item'>
                  <div className='right-feature-item-icon'>
                    <i className='fa fa-certificate'></i>
                  </div>

                  <h3> Feature Four</h3>

                  <p>
                    Lorem ipsum dolor, consectetur sed do adipisicing elit, sed
                    do eiusmod tempor incididunt
                  </p>
                </div>

                <div className='right-feature-item'>
                  <div className='right-feature-item-icon'>
                    <i className='fa fa-rss'></i>
                  </div>

                  <h3> Feature Five</h3>

                  <p>
                    Lorem ipsum dolor, consectetur sed do adipisicing elit, sed
                    do eiusmod tempor incididunt
                  </p>
                </div>

                <div className='right-feature-item'>
                  <div className='right-feature-item-icon'>
                    <i className='fa fa-database'></i>
                  </div>

                  <h3> Feature Six</h3>

                  <p>
                    Lorem ipsum dolor, consectetur sed do adipisicing elit, sed
                    do eiusmod tempor incididunt
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className='stats'>
        <div className='container'>
          <div className='row'>
            <div className='col-md-12'>
              <div className='section-header wow fadeIn' data-wow-duration='1s'>
                <h1 className='wallet-header'> Wallets </h1>

                <p className='wallet-header-desc'>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                  Ut enim ad minim veniam, quis nostrud exercitation ullamco
                  laboris nisi ut aliquip.
                </p>
              </div>
            </div>
          </div>
          <div className='row'>
            <div className='col-md-3'>
              <div className='wallet-card text-center'>
                <img
                  className='wallet-image'
                  alt='wallet'
                  src='https://img.icons8.com/color/48/000000/wallet.png'
                />
                <h4>Wallet One</h4>
                <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit.</p>
              </div>
            </div>
            <div className='col-md-3'>
              <div className='wallet-card text-center'>
                <img
                  className='wallet-image'
                  alt='wallet'
                  src='https://img.icons8.com/color/48/000000/wallet.png'
                />
                <h4>Wallet Two</h4>
                <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit.</p>
              </div>
            </div>
            <div className='col-md-3'>
              <div className='wallet-card text-center'>
                <img
                  className='wallet-image'
                  alt='wallet'
                  src='https://img.icons8.com/color/48/000000/wallet.png'
                />
                <h4>Wallet Three</h4>
                <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit.</p>
              </div>
            </div>
            <div className='col-md-3'>
              <div className='wallet-card text-center'>
                <img
                  className='wallet-image'
                  alt='wallet'
                  src='https://img.icons8.com/color/48/000000/wallet.png'
                />
                <h4>Wallet Four</h4>
                <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit.</p>
              </div>
            </div>
            <div className='col-md-3'>
              <div className='wallet-card text-center'>
                <img
                  className='wallet-image'
                  alt='wallet'
                  src='https://img.icons8.com/color/48/000000/wallet.png'
                />
                <h4>Wallet Five</h4>
                <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit.</p>
              </div>
            </div>
            <div className='col-md-3'>
              <div className='wallet-card text-center'>
                <img
                  className='wallet-image'
                  alt='wallet'
                  src='https://img.icons8.com/color/48/000000/wallet.png'
                />
                <h4>Wallet Six</h4>
                <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit.</p>
              </div>
            </div>
            <div className='col-md-3'>
              <div className='wallet-card text-center'>
                <img
                  className='wallet-image'
                  alt='wallet'
                  src='https://img.icons8.com/color/48/000000/wallet.png'
                />
                <h4>Wallet Seven</h4>
                <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit.</p>
              </div>
            </div>
            <div className='col-md-3'>
              <div className='wallet-card text-center'>
                <img
                  className='wallet-image'
                  alt='wallet'
                  src='https://img.icons8.com/color/48/000000/wallet.png'
                />
                <h4>Wallet Eight</h4>
                <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit.</p>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className='team'>
        <div className='container'>
          <div className='row'>
            <div className='col-md-12'>
              <div className='section-header'>
                <h1 className='text-center'>Team</h1>
              </div>
            </div>
          </div>
          <div className='row'>
            <div className='col-sm-3 cus-padding'>
              <div>
                <div className='customer-svg'>
                  <img
                    className='cus-img-bckgrd1'
                    src='https://img.icons8.com/color/50/000000/user.png'
                    alt='team-member'
                  />
                </div>
                <p className='cus-p'>
                  "Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                  Vitae harum architecto repellat natus tenetur sint alias
                  dolore praesentium, iure quis commodi inventore ab culpa
                  libero! Provident possimus accusamus cupiditate pariatur!"
                </p>
                <h4>John Doe</h4>
              </div>
            </div>
            <div className='col-sm-3 cus-padding'>
              <div>
                <div className='customer-svg'>
                  <img
                    className='cus-img-bckgrd1'
                    src='https://img.icons8.com/color/50/000000/user.png'
                    alt='team-member'
                  />
                </div>
                <p className='cus-p'>
                  "Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                  Vitae harum architecto repellat natus tenetur sint alias
                  dolore praesentium, iure quis commodi inventore ab culpa
                  libero! Provident possimus accusamus cupiditate pariatur!"
                </p>
                <h4>John Doe</h4>
              </div>
            </div>
            <div className='col-sm-3 cus-padding'>
              <div className='customer-svg'>
                <img
                  className='cus-img-bckgrd1'
                  src='https://img.icons8.com/color/50/000000/user.png'
                  alt='team-member'
                />
              </div>
              <div>
                <p className='cus-p'>
                  "Lorem ipsum, dolor sit amet consectetur adipisicing elit.{' '}
                  <br />
                  <br /> Vitae harum architecto repellat natus tenetur sint
                  alias dolore praesentium, iure quis commodi inventore ab culpa
                  libero! <br />
                  <br /> Provident possimus accusamus cupiditate pariatur!"
                </p>
                <h4>John Doe</h4>
              </div>
            </div>
            <div className='col-sm-3 cus-padding'>
              <div className='customer-svg'>
                <img
                  className='cus-img-bckgrd1'
                  src='https://img.icons8.com/color/50/000000/user.png'
                  alt='team-member'
                />
              </div>
              <div>
                <p className='cus-p'>
                  "Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                  Vitae harum architecto repellat natus tenetur sint alias
                  dolore praesentium, iure quis commodi inventore ab culpa
                  libero! Provident possimus accusamus cupiditate pariatur!"
                </p>
                <h4>John Doe</h4>
              </div>
            </div>
          </div>
        </div>
      </section>
    </Fragment>
  );
};

export default Landing;
