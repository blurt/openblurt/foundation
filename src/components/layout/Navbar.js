import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import Logo from '../images/logo.png';

const Navbar = () => {
  return (
    <Fragment>
      <nav
        className='navbar navbar-expand-lg navbar-light padding-nav'
        style={navStyling}
      >
        <Link className='navbar-brand' to='/'>
          <img width='70' src={Logo} alt='Blurt_Logo' />
        </Link>
        <button
          className='navbar-toggler'
          type='button'
          data-toggle='collapse'
          data-target='#navbarSupportedContent'
          aria-controls='navbarSupportedContent'
          aria-expanded='false'
          aria-label='Toggle navigation'
        >
          <span className='navbar-toggler-icon'></span>
        </button>

        <div
          className='collapse navbar-collapse ml-auto'
          id='navbarSupportedContent'
        >
          <ul className='navbar-nav mr-auto ml-auto'>
            <li className='nav-item active'>
              <Link className='nav-link' to='/'>
                Home <span className='sr-only'>(current)</span>
              </Link>
            </li>
            <li className='nav-item'>
              <Link className='nav-link' to='/foundation'>
                Foundation
              </Link>
            </li>
            <li className='nav-item'>
              <Link className='nav-link' to='/about'>
                About
              </Link>
            </li>
          </ul>
        </div>
      </nav>
    </Fragment>
  );
};
const navStyling = {
  // padding: '20px 25px!important'
  padding: '50px!important',
  backgroundColor: 'transparent',
};

export default Navbar;
